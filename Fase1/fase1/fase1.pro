TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    comandos.c \
    discos.c \
    reportes.c \
    montar.c

HEADERS += \
    comandos.h \
    discos.h \
    reportes.h \
    montar.h

