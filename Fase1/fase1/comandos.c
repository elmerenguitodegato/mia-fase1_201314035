#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "comandos.h"


typedef struct Datos{
    char valor[100];
}Datos;


char valp1[100]; char valp2[100]; char valp3[100];
char valp6[100]; char valp5[100]; char valp4[100];

char par1[10]; char par2[10]; char par3[10];
char par6[10]; char par5[10]; char par4[10];

char *fgets(char *s, int n, FILE *stream);

void minusculas(char *s)
{
    while(*s!='\0')
    {
        if(*s>='A' && *s<='Z')
            *s+=(char)32;
        s++;
    }
}

int ContadorCaracteres(char *cad){
     int i=0;
     while (cad[i]!='\0'){
           i++;
     }
     return(i);
}

int cantidadParametros(char* cad){
    int i = 0;
    int cant =0;
    bool comillas = false;

    while(cad[i]!='\0'){
        if(comillas == false){
            if(cad[i]== ' '){
                if(cad[i+1] != ' ') cant++;
            }else
            if(cad[i]== '"'){
                comillas = true;
            }
        }else{
            if(cad[i]=='"')comillas =false;
        }
        i++;
    }
//    printf("\nse recibieron %i paratemtros \n", cant);
    return cant;
}

void imprimirValores(char* cad){
    int i=0;

    int i1 =0;
    int i2 =0;
    int i3 =0;
    int i4=0;
    int i5=0;
    int i6=0;

    bool si;
    si =false;
    int indicador= 0;

    bool comillas = false;

    //recorre la cadena y obtine los valores de los parametros
      while(cad[i]!='\0'){
          if(comillas == false){
              if(cad[i]=='='){
                  indicador++;
                  i++;
                  si=true;

                  if(cad[i]== '"') comillas =true;
              }else
              if(cad[i]==' '){
                  si = false;
              }
          }else{
              if(cad[i]== '"') comillas =false;
          }


          if(si){
              if(indicador ==1){
                  valp1[i1] = cad[i];
                  i1++;
                  valp1[i1] = '\0';
              }else
              if(indicador == 2){
                  valp2[i2] = cad[i];
                  i2++;
                  valp2[i2] = '\0';
              }else
              if(indicador ==3){
                  valp3[i3] = cad[i];
                  i3++;
                  valp3[i3] = '\0';
              }else
              if(indicador ==4){
                  valp4[i4] = cad[i];
                  i4++;
                  valp4[i4] = '\0';
              }else
              if(indicador ==5){
                  valp5[i5] = cad[i];
                  i5++;
                  valp5[i5] = '\0';
              }else
              if(indicador ==6){
                  valp6[i6] = cad[i];
                  i6++;
                  valp6[i6] = '\0';
              }
          }

          i++;
      }

 //     printf("\n%s, %s, %s, %s, %s, %s\n",valp1,valp2, valp3, valp4, valp5, valp6);
}

void imprimirParametros(char* cad){
    int i=0;

    int i1 =0;
    int i2 =0;
    int i3 =0;
    int i4 =0;
    int i5=0;
    int i6=0;

    bool si=false;
    int indicador= 0;

    bool comillas = false;

    //recorre la cadena y obtienen los nombres de los parametros
    while(cad[i]!='\0'){

        if(comillas == false){
            if(cad[i]==' '){
                if(cad[i+1] != ' '){
                    indicador++;
                    i++;
                    si=true;
                }
            }else
            if(cad[i]=='='){
                si = false;
                if(cad[i+1]=='"') comillas = true;
            }
        }else{
            if(cad[i+1]=='"')comillas =false;
        }

        if(si){
            if(indicador ==1){
                par1[i1] = cad[i];
                i1++;
                par1[i1] = '\0';
            }else
            if(indicador == 2){
                par2[i2] = cad[i];
                i2++;
                par2[i2] = '\0';
            }else
            if(indicador ==3){
                par3[i3] = cad[i];
                i3++;
                par3[i3] = '\0';
            }else
            if(indicador ==4){
                par4[i4] = cad[i];
                i4++;
                par4[i4] = '\0';
            }else
            if(indicador ==5){
                par5[i5] = cad[i];
                i5++;
                par5[i5] = '\0';
            }else
            if(indicador ==6){
                par6[i6] = cad[i];
                i6++;
                par6[i6] = '\0';
            }

        }

        i++;
    }

 //   printf("\n%s, %s, %s, %s, %s, %s\n",par1,par2, par3,par4,par5,par6);

}

void ordenarParametros(char* cadena){

    char cad[500];
    strcpy(cad,cadena);
    minusculas(cad);
    //en este metodo trabajo todo con la cadena de entrada
    imprimirParametros(cad);
    imprimirValores(cad);

    char comando[15];

    int j;
    for(j=0; j<14; j++){
        if(cad[j]== ' '){
            break;
        }else{
            comando[j] = cad[j];
            comando[j+1] = '\0';
        }
    }

 //   printf("\nComando ingresado = %s\n",comando);

    Datos valores[6];
    Datos parametros[6];

    strcpy(valores[0].valor,valp1);
    strcpy(valores[1].valor,valp2);
    strcpy(valores[2].valor,valp3);
    strcpy(valores[3].valor,valp4);
    strcpy(valores[4].valor,valp5);
    strcpy(valores[5].valor,valp6);


    strcpy(parametros[0].valor,par1);
    strcpy(parametros[1].valor,par2);
    strcpy(parametros[2].valor,par3);
    strcpy(parametros[3].valor,par4);
    strcpy(parametros[4].valor,par5);
    strcpy(parametros[5].valor,par6);

    int numParemetros = cantidadParametros(cad);

    //COMANDO MKDISK
    if(strcmp(comando,"mkdisk")==0){
        char size[15]= "null\0"; char unit[2]= "m\0"; char path[100]= "null\0";
        int i;
        for(i=0; i<numParemetros; i++){
            if(strcmp(parametros[i].valor,"-size")==0){
                strcpy(size,valores[i].valor);
            }else
            if(strcmp(parametros[i].valor,"-unit")==0){
                strcpy(unit,valores[i].valor);
            }else
            if(strcmp(parametros[i].valor,"-path")==0){
                strcpy(path,valores[i].valor);
            }
        }

        if(strcmp(size,"null")==0 || strcmp(path,"null")==0){
            printf("\n***ERROR EN LOS PARAMETROS***\n");
        }else {
          //  printf("\ncrearDisco(%s,%s,%s)\n",size,unit,path);
            crearDisco(atoi(size),path,unit);
        }
    }else
    //COMANDO RMDISK
    if(strcmp(comando,"rmdisk")==0){
        if(strcmp(parametros[0].valor,"-path")==0){
          //  printf("\nborrardisco(%s)\n",valores[0].valor);
            eliminarDiscos(valores[0].valor);
        }else{
            printf("\n***ERROR EN LOS PARAMETROS***\n");
        }
    }else
    //COMANDO FDISK
    if(strcmp(comando,"fdisk")==0){
        char size[15]="null\0"; char unit[2]="k\0"; char path[100]="null\0"; char type[2]="p\0";
        char fit[3] = "wf\0"; char delete_[5]="null"; char name[30]="null"; char add[15]= "null";
        int i;
        for(i=0; i<numParemetros; i++){
            if(strcmp(parametros[i].valor,"-size")==0){
                strcpy(size,valores[i].valor);
            }else
            if(strcmp(parametros[i].valor,"-unit")==0){
                strcpy(unit,valores[i].valor);
            }else
            if(strcmp(parametros[i].valor,"-path")==0){
                strcpy(path,valores[i].valor);
            }else
            if(strcmp(parametros[i].valor,"-type")==0){
                strcpy(type,valores[i].valor);
            }else
            if(strcmp(parametros[i].valor,"-fit")==0){
                strcpy(fit,valores[i].valor);
            }else
            if(strcmp(parametros[i].valor,"-delete")==0){
                strcpy(delete_,valores[i].valor);
            }else
            if(strcmp(parametros[i].valor,"-name")==0){
                strcpy(name,valores[i].valor);
            }else
            if(strcmp(parametros[i].valor,"-add")==0){
                strcpy(add,valores[i].valor);
            }
        }

        if(strcmp(delete_,"null")==0 && strcmp(add,"null")==0){
            if(strcmp(size,"null")==0 || strcmp(name,"null")==0 || strcmp(path,"null")==0){
                printf("\n***ERROR EN LOS PARAMETROS***\n");
            }else{
              //  printf("\nCrearParticion(%s,%s,%s,%s,%s,%s);\n",size,unit,path,type,fit,name);
                crearParticiones(atoi(size),path,name,unit,type,fit);
            }
        }else
        if(strcmp(delete_,"null")==0){
            printf("\nAgregarEspacio(%s,%s,%s,%s);\n",add,unit,path,name);
            formatearParticion(name,path,delete_);
        }else
        if(strcmp(add,"null")==0){
            printf("\nFormatearParticion(%s,%s,%s)\n",delete_,path,name);
            formatearParticion(name,path,delete_);
        }

    }else
    //COMANDO MOUNT
    if(strcmp(comando,"mount")==0){
        char name[30]= "null\0"; char path[100]="null\0";
        int i;
        for(i=0; i<numParemetros; i++){
            if(strcmp(parametros[i].valor,"-name")==0){
                strcpy(name,valores[i].valor);
            }else
            if(strcmp(parametros[i].valor,"-path")==0){
                strcpy(path,valores[i].valor);
            }
        }

        if(strcmp(path,"null")==0 || strcmp(name,"null")==0){
            printf("\n***ERROR EN LOS PARAMETROS***\n");
        }else{
            montarParticion(path, name);
        }
    }else
    //COMANDO UNMOUNT
    if(strcmp(comando,"unmount")==0){
        char id[150]= "null\0";
        int i;
        for(i=0; i<numParemetros; i++){
            if(strcmp(parametros[i].valor,"-id")==0){
                strcpy(id,valores[i].valor);
            }
        }
        if(strcmp(id,"null")==0){
            printf("\n***PARAMETROS INCORECTOS***\n");
        }else{
            printf("id recibido = %s\n",id);
             desmontarParticion(id);
        }
    }else
    //COMANDO EXEC
    if(strcmp(comando,"exec")==0){
        char ruta[100];
        strcpy(ruta,"/home/estuardo/archivos/hola2.txt");
        FILE* script = fopen(ruta,"r");
        if(script == NULL){
            printf("\n***EL SCRIPT INDICADO NO EXISTE***\n");
        }else{
            char line[100];
            while (fgets(line, 99, script) != NULL)
            {
               /* Aquí tratamos la línea leída */

               if(line[0] !='#'){
                   if(strcmp(line,"\n") !=0){
                       int t;
                       int index = ContadorCaracteres(line);
                       char linea[index];
                       line[index-1]='\0';
                    //   printf("%s\n",line);

                       for(t=0; t<index; t++){
                           if(line[t]=='#'){
                               linea[t] = '\0';
                               break;
                           }
                           linea[t] = line[t];
                       }
                       printf("================================================================================\n");
                       printf("\n%s\n",linea);
                       ordenarParametros(linea);
                       printf("================================================================================\n");
                   }
               }
            }
        }
    }else
    //COMANDO REP
    if(strcmp(comando,"rep")==0){
        char id[5]= "null\0"; char name[32]= "null\0"; char path[150]= "null\0";
        int i;
        for(i=0; i<numParemetros; i++){
            if(strcmp(parametros[i].valor,"-name")==0){
                strcpy(name,valores[i].valor);
            }
            if(strcmp(parametros[i].valor,"-id")==0){
                strcpy(id,valores[i].valor);
            }
            if(strcmp(parametros[i].valor,"-path")==0){
                strcpy(path,valores[i].valor);
            }
        }

        if(strcmp(id,"null")==0 || strcmp(path,"null")==0 || strcmp(name,"null")==0){
            printf("\n***ERROR EN LOS PARAMETROS***\n");
        }else{
            if(strcmp(name,"mbr")==0)reporteMBR(path,id);
            else
            if(strcmp(name,"disk")==0)reporteDisco(path,id);
            else{
                printf("***\nEL PARAMETRO -NAME SOLO ACEPTA COMO VALOR: MBR | DISK\n***");
            }

        }
    }else
    //COMANDO EXIT
    if(strcmp(comando,"exit")==0){
        exit(0);
    }
    //COMANDO NO RECONOCIDO
    else{
        printf("\n***COMANDO \"%s\" INVALIDO***\n",comando);
    }
}

void esperarComandos(){
    char entrada[500];
        printf("********************************************************************************\n");
        printf("INGRESAR COMANDO: ==> ");
        scanf(" %99[^\n]", entrada);
    //    printf("%s\n",entrada);

        ordenarParametros(entrada);
        printf("================================================================================\n");
        //se llama a si mismo
        esperarComandos();
}

/*
 * mkdisk -size=6 -unit=k -path=hola
 * mkdisk -path="/home/estuardo/nueva carpeta/" -size=5
 *
 * fdisk -size=5 -unit=k -path=ruta -type=p -fit=bb -name=p1
 *
 *
 * */

