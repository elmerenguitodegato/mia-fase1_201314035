#include "discos.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void crearDisco(int tamanio, char* ruta, char* unidad){

    MasterBootRecord mbr;
    FILE* existe;
    existe = fopen(ruta,"rb");

    if(existe==NULL){
        FILE* archivo;
        archivo = fopen(ruta,"wt");

        if(archivo == NULL){
                     printf("\n***ERROR AL CREAR EL DISCO**\n");
                 }else{
                     int kbyte = 1024;
                     int Mbyte = kbyte*kbyte;
                     char basura1[Mbyte];
                     char basura2[kbyte];

                     int i;
                     if(strcmp(unidad,"m")==0){
                         for(i = 0; i<tamanio; i++){
                             fwrite(basura1,sizeof(basura1),1,archivo);
                         }
                         mbr.mbr_tamanio = tamanio*1024*1024;
                     }else
                     if(strcmp(unidad,"k")==0){
                         for(i = 0; i<tamanio; i++){
                             fwrite(basura2,sizeof(basura2),1,archivo);
                         }
                         mbr.mbr_tamanio = tamanio*1024;
                     }

                     mbr.mbr_asignature = rand() % (100+1);

                     for(i=0; i<4; i++){
                         mbr.particones[i].status = '0';
                         mbr.particones[i].type = '0';
                         mbr.particones[i].start = -1;
                         mbr.particones[i].fit = '0';
                         mbr.particones[i].size =-1;
                         strcpy(mbr.particones[i].name,"null");
                     }


                     time_t tiempo = time(0);
                     struct tm *tlocal = localtime(&tiempo);

                     //strftime(mbr.fecha_Creacion,128,"%d/%m/%y %H:%M:%S",tlocal);
                     //printf("%s\n",mbr.fecha_Creacion);

                     fseek(archivo,0,SEEK_SET);
                     fwrite(&mbr,sizeof(mbr),1,archivo);

                     fclose(archivo);
                     printf("\n***DISCO CREADO CON EXITO***\n");
                 }

    }else{
        fclose(existe);
        printf("\n***YA EXISTE UN DISCO CON EL MISMO NOMBRE EN EL DIRECTORIO INDICADO***\n");
    }

}

void eliminarDiscos(char* ruta){
    FILE* archivo;

    archivo = fopen(ruta,"rb");
    if(archivo == NULL){
        printf("\n***NO EXISTE EL DISCO INDICADO***\n");
    }else{
        fclose(archivo);
        int devuelve = remove(ruta);

        if(devuelve==0){
            printf("\n***DISCO ELIMINADO CON EXITO***\n");
        }
    }
}

/*
 * PARTICOINES
 *
 * */

int numBytes(char* unit, int size){
    int sizeP = 0;
    if(strcmp(unit,"b")==0){
        sizeP=size;
    }else
    if(strcmp(unit,"k")==0){
        sizeP=size*1024;
    }else
    if(strcmp(unit,"m")==0){
        sizeP=size*1024*1024;
    }
    return sizeP;
}

int CantidadPrimarias(char* ruta){
    int cont=0;
    FILE* disco;
    disco = fopen(ruta,"r");

    if(disco == NULL){
        printf("\n***ERROR AL ABRIR EL DISCO***\n");
    }else{
        MasterBootRecord mbr;
        fseek(disco,0,SEEK_SET);
        fread(&mbr,sizeof(mbr),1,disco);
        int i;
        for(i=0; i<4; i++){
            if(mbr.particones[i].type== 'p' && mbr.particones[i].status == '1'){
                cont++;
            }
        }
        fclose(disco);
    }
    return cont;
}

int CantidadExtendidas(char* ruta){
    int cont=0;
    FILE* disco;
    disco = fopen(ruta,"r");

    if(disco == NULL){
        printf("\n***ERROR AL ABRIR EL DISCO***\n");
    }else{
        MasterBootRecord mbr;
        fseek(disco,0,SEEK_SET);
        fread(&mbr,sizeof(mbr),1,disco);
        int i;
        for(i=0; i<4; i++){
            if(mbr.particones[i].type== 'e' && mbr.particones[i].status == '1'){
                cont++;
            }
        }
        fclose(disco);
    }
    return cont;
}

int tamanioLibreDisco(char* ruta){
    int tam=0;

    FILE* disco;
    disco = fopen(ruta,"r");

    if(disco == NULL){
        printf("\n***ERROR AL ABRIR EL DISCO***\n");
    }else{
        MasterBootRecord mbr;
        fseek(disco,0,SEEK_SET);
        fread(&mbr,sizeof(mbr),1,disco);
        int i;
        for(i=0; i<4; i++){
            if(mbr.particones[i].status == '1'){
                tam+=mbr.particones[i].size;
            }
        }
        tam = mbr.mbr_tamanio - tam - sizeof(mbr);
        fclose(disco);
    }
    return tam;
}

int VerificarNombre(char* ruta, char* name){
    int existe=0;
    FILE* disco;
    disco = fopen(ruta,"r");

    if(disco == NULL){
        printf("\n***ERROR AL ABRIR EL DISCO***\n");
    }else{
        MasterBootRecord mbr;
        fseek(disco,0,SEEK_SET);
        fread(&mbr,sizeof(mbr),1,disco);
        int i;
        for(i=0; i<4; i++){
            if(strcmp(mbr.particones[i].name,name)==0){
                existe=1;
            }
        }
        fclose(disco);
    }
    return existe;
}

int byteStart(char* ruta, int tamParticion){
    MasterBootRecord mbr;
    int inicio= sizeof(mbr);
    FILE* disco;
    disco = fopen(ruta,"r");

    if(disco == NULL) printf("\n***ERROR AL ABRIR EL DISCO***\n");
    else{
        fseek(disco,0,SEEK_SET);
        fread(&mbr,sizeof(mbr),1,disco);
        int numPrimarias = CantidadPrimarias(ruta);
        int numExtendidas = CantidadExtendidas(ruta);
        if((numPrimarias+numExtendidas) ==0 ) return inicio;
        int i;
        for(i=0;i<4; i++){
            if(mbr.particones[i].status=='1'){
                if(tamParticion<=(mbr.particones[i].start-inicio)){
                    return inicio;
                }
                inicio = mbr.particones[i].start+mbr.particones[i].size;
            }
        }
        return inicio;
    }
    return -1;
}

int byteStartLogica(char* ruta){
    MasterBootRecord mbr;
    FILE* disco;
    disco = fopen(ruta,"r");

    if(disco == NULL) printf("\n***ERROR AL ABRIR EL DISCO***\n");
    else{
        fseek(disco,0,SEEK_SET);
        fread(&mbr,sizeof(mbr),1,disco);

        int i;
        for(i=0;i<4; i++){
            if(mbr.particones[i].type=='e'){
                return mbr.particones[i].start;
            }
        }
    }
    return -1;
}


void colocarAdjuntos(char* ruta){
    FILE* disco;
    disco = fopen(ruta,"rb+");

    if(disco==NULL)printf("\n***ERROR AL ABRIR EL DISCO***\n");
    else{
        MasterBootRecord mbr;

        fseek(disco,0,SEEK_SET);
        fread(&mbr,sizeof(mbr),1,disco);
        int i;
        for(i=0; i<4; i++){
            if(mbr.particones[i].status == '0'){
                if(i==0){
                    mbr.particones[0]= mbr.particones[1];
                    mbr.particones[1]= mbr.particones[2];
                    mbr.particones[2]= mbr.particones[3];
                    mbr.particones[3].status = '0';
                    mbr.particones[3].start = -1;
                    printf("\n***DESFRAGMENTACION 1***\n");
                }else
                if(i==1){
                    mbr.particones[1]= mbr.particones[2];
                    mbr.particones[2]= mbr.particones[3];
                    mbr.particones[3].status = '0';
                    mbr.particones[3].start = -1;
                    printf("\n***DESFRAGMENTACION 2***\n");
                }
                if(i==2){
                    mbr.particones[2]= mbr.particones[3];
                    mbr.particones[3].status = '0';
                    mbr.particones[3].start = -1;
                    printf("\n***DESFRAGMENTACION 3***\n");
                }
            }
        }
        fseek(disco,0,SEEK_SET);
        fwrite(&mbr,sizeof(mbr),1,disco);
        fclose(disco);
      //  printf("\n***DESFRAGMENTACION CON EXITO***\n");
    }
}

void ordenarParticiones(char* ruta){
    FILE* disco;
    disco = fopen(ruta,"rb+");

    if(disco==NULL)printf("\n***ERROR AL ABRIR EL DISCO***\n");
    else{
        MasterBootRecord mbr;

        fseek(disco,0,SEEK_SET);
        fread(&mbr,sizeof(mbr),1,disco);

        int j;  int i;
        for(i = 0; i < 4 - 1; i++){
            for(j = 0; j < 4 - 1; j++){
                if (mbr.particones[j].start > mbr.particones[j + 1].start){
                    if(mbr.particones[j + 1].start != -1){
                        Partition tmp = mbr.particones[j];
                        mbr.particones[j] = mbr.particones[j+1];
                        mbr.particones[j+1] = tmp;
                    }
                }
            }
        }

        fseek(disco,0,SEEK_SET);
        fwrite(&mbr,sizeof(mbr),1,disco);
        fclose(disco);
        printf("\n***ORDENAMIENTO CON EXITO***\n");

        }

}

void crearParticionPrimaria(int size, char* ruta, char* name, char* unit, char* type, char* fit){

    FILE* disco;
    disco = fopen(ruta,"rb+");

    if(disco== NULL) printf("\n***EL DISCO INDICADO NO EXISTE***\n");
    else{
        int tamParticion = numBytes(unit,size);
        int tamLibreDisco = tamanioLibreDisco(ruta);
        if(tamParticion<=tamLibreDisco){
            int existe = VerificarNombre(ruta,name);
            if(existe == 0){
                int numPrimarias = CantidadPrimarias(ruta);
                int numExtendidas = CantidadExtendidas(ruta);

                if(numPrimarias < 4){
                    if((numExtendidas + numPrimarias)<4){
                       int byteInicio = byteStart(ruta,tamParticion);
                        Partition primaria;
                        primaria.fit = fit[0];
                        strcpy(primaria.name,name);
                        primaria.size = tamParticion;
                        primaria.start = byteInicio;
                        primaria.status = '1';
                        primaria.type = type[0];

                        MasterBootRecord mbr;
                        fseek(disco,0,SEEK_SET);
                        fread(&mbr,sizeof(mbr),1,disco);
                        mbr.particones[numPrimarias+numExtendidas] = primaria;

                        //
                        printf("\nbyte start %i\n",primaria.start);
                        printf("tamanio %i\n",primaria.size);

                        //
                        fseek(disco,0,SEEK_SET);
                        fwrite(&mbr,sizeof(mbr),1,disco);
                        fclose(disco);
                        printf("\n***PARTICION CREADA CON EXITO***\n");
                    }else printf("\n***NO SE PUEDE CREAR LA PARTICION***\n***YA EXISTEN TRES PARTICIONES PRIMARIAS Y UNA EXTENDIDA***\n");
                }else printf("\n***NO SE PUEDE CREAR LA PARTICION***\n***YA EXISTEN CUATRO PARTICIONES PRIMARIAS***\n");
            }else printf("\n***NO SE PUEDE CREAR LA PARTICION***\n***YA EXISTE UNA PARTICION CON EL NOMBRE INDICADO***\n");
        }else printf("\n***NO SE PUEDE CREAR LA PARTICION****\n***ESPACIO INSUFICIENTE EN EL DISCO***\n");

    }
}

void crearParticonExtendida(int size, char* ruta, char* name, char* unit, char* type, char* fit){
    FILE* disco;
    disco = fopen(ruta,"rb+");

    if(disco== NULL) printf("\n***EL DISCO INDICADO NO EXISTE***\n");
    else{
        int tamParticion = numBytes(unit,size);
        int tamLibreDisco = tamanioLibreDisco(ruta);
        if(tamParticion<=tamLibreDisco){
            int existe = VerificarNombre(ruta,name);
            if(existe == 0){
                int numPrimarias = CantidadPrimarias(ruta);
                int numExtendidas = CantidadExtendidas(ruta);

                if(numExtendidas < 1){
                    if((numPrimarias+numExtendidas) < 4){
                       int byteInicio = byteStart(ruta,tamParticion);
                        Partition primaria;
                        primaria.fit = fit[0];
                        strcpy(primaria.name,name);
                        primaria.size = tamParticion;
                        primaria.start = byteInicio;
                        primaria.status = '1';
                        primaria.type = type[0];

                        MasterBootRecord mbr;
                        fseek(disco,0,SEEK_SET);
                        fread(&mbr,sizeof(mbr),1,disco);
                        mbr.particones[numPrimarias] = primaria;

                        fseek(disco,0,SEEK_SET);
                        fwrite(&mbr,sizeof(mbr),1,disco);

                        ExtendBootRecord ebr;

                        ebr.fit = fit[0];
                        strcpy(ebr.name," ");
                        ebr.next = -1;
                        ebr.size = 0;
                        ebr.start = byteInicio;
                        ebr.status = '0';

                        fseek(disco,byteInicio,SEEK_SET);
                        fwrite(&mbr,sizeof(mbr),1,disco);

                        fclose(disco);
                        printf("\n***PARTICION CREADA CON EXITO***\n");
                    }else printf("\n***NO SE PUEDE CREAR LA PARTICION***\n***YA EXISTEN TRES PARTICIONES PRIMARIAS Y UNA EXTENDIDA***\n");
                }else printf("\n***NO SE PUEDE CREAR LA PARTICION***\n***YA EXISTEN UNA PARTICION EXTENDIDA***\n");
            }else printf("\n***NO SE PUEDE CREAR LA PARTICION***\n***YA EXISTE UNA PARTICION CON EL NOMBRE INDICADO***\n");
        }else printf("\n***NO SE PUEDE CREAR LA PARTICION****\n***ESPACIO INSUFICIENTE EN EL DISCO***\n");

    }
}

void crearParticionLogica(int size, char* ruta, char* name, char* unit, char* type, char* fit){
    FILE* disco;
    disco = fopen(ruta,"rb+");

    if(disco== NULL) printf("\n***EL DISCO INDICADO NO EXISTE***\n");
    else{
        int tamParticion = numBytes(unit,size);
        int numExtendidas = CantidadExtendidas(ruta);

        if(numExtendidas ==1){
             int byteInicio = byteStartLogica(ruta);
             ExtendBootRecord ebr;

             fseek(disco,byteInicio,SEEK_SET);
             fread(&ebr,sizeof(ebr),1,disco);

             if(ebr.next==-1){
                 ebr.fit = fit[0];
                 strcpy(ebr.name,name);
                 ebr.next = sizeof(ebr)+tamParticion;
                 ebr.size = tamParticion;
                 ebr.start = byteInicio;
                 ebr.status = '1';

                 fseek(disco,byteInicio,SEEK_SET);
                 fwrite(&ebr,sizeof(ebr),1,disco);
             }else{
                 int bandera = ebr.next;
/*
                 while(true){
                     fseek(disco,bandera,SEEK_SET);
                     fread(&ebr,sizeof(ebr),1,disco);

                     bandera = ebr.next;
                     if(ebr.next == -1){
                         break;
                     }
                 }
                 */
             }



             fseek(disco,byteInicio,SEEK_SET);
             fwrite(&ebr,sizeof(ebr),1,disco);


             fclose(disco);
             printf("\n***PARTICION CREADA CON EXITO***\n");
        }else printf("\n***NO SE PUEDE CREAR LA PARTICION***\n***NO EXISTE UNA PARTICION EXTENDIDA***\n");

    }
}

void crearParticiones(int size, char* ruta, char* name, char* unit, char* type, char* fit){
    colocarAdjuntos(ruta);
    if(strcmp(type,"p")==0){
        crearParticionPrimaria(size,ruta,name,unit,type,fit);
 //       ordenarParticiones(ruta);
    }
    else
    if(strcmp(type,"l")==0){
    //    particionLogica(size,ruta,name,unit,fit);
    }else
    if(strcmp(type,"e")==0){
        crearParticonExtendida(size,ruta,name,unit,type,fit);
    }
    ordenarParticiones(ruta);
    //    particionExtendida(size,ruta,name,unit,fit);

}

void formatearFast(char* name, char* path){
    FILE* disco;
    disco = fopen(path,"rb+");

    if(disco==NULL)printf("***\nEL DISCO INDICADO NO EXISTE***\n");
    else{
        MasterBootRecord mbr;
        fseek(disco,0,SEEK_SET);
        fread(&mbr,sizeof(mbr),1,disco);
        int existe=0;

        int i;
        for(i=0; i<4; i++){
            if(mbr.particones[i].status == '1' && strcmp(mbr.particones[i].name,name)==0){
                mbr.particones[i].status = '0';
                fseek(disco,0,SEEK_SET);
                fwrite(&mbr,sizeof(mbr),1,disco);
                existe =1;
                printf("\n***PARTICION FORMATEADA CON EXITO***\n");
            }
        }
        fclose(disco);
        if(existe ==0){
            printf("\n***LA PARTICION INDICADA NO EXISTE***\n");
        }
    }
}

void formatearFull(char* name, char* path){
    FILE* disco;
    disco = fopen(path,"rb+");

    if(disco==NULL)printf("***\nEL DISCO INDICADO NO EXISTE***\n");
    else{
        MasterBootRecord mbr;
        fseek(disco,0,SEEK_SET);
        fread(&mbr,sizeof(mbr),1,disco);
        int existe=0;

        int i;
        for(i=0; i<4; i++){
            if(mbr.particones[i].status == '1' && strcmp(mbr.particones[i].name,name)==0){
                mbr.particones[i].status = '0';
                fseek(disco,0,SEEK_SET);
                fwrite(&mbr,sizeof(mbr),1,disco);
                existe =1;

                int ini = mbr.particones[i].start;
                int fin = ini+mbr.particones[i].start;
                int t;

                char basura = '\0';
                for(t=ini; t<fin; t++){
                    fseek(disco,t,SEEK_SET);
                    fwrite(&basura,sizeof(basura),1,disco);
                }

                printf("\n***PARTICION FORMATEADA CON EXITO***\n");
            }
        }
        fclose(disco);
        if(existe ==0){
            printf("\n***LA PARTICION INDICADA NO EXISTE***\n");
        }
    }
}

void formatearParticion(char* name, char* path, char* tipo){
    if(strcmp(tipo,"fast")==0){
        formatearFast(name,path);
    }
    else
    if(strcmp(tipo,"full")==0){
        formatearFull(name,path);
    }
    colocarAdjuntos(path);
    ordenarParticiones(path);
  //  desmontarParticionDos(name, path);
}
