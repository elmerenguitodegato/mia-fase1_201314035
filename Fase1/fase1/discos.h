#ifndef DISCOS_H
#define DISCOS_H
#include "montar.h"

typedef struct Partition{
    char status;
    char type;
    char fit;
    int start;
    int size;
    char name[16];
}Partition;

typedef struct MasterBootRecord{
    int mbr_tamanio;
    char fecha_Creacion[15];
    int mbr_asignature;
    Partition particones[4];
}MasterBootRecord;

typedef struct ExtendBootRecord{
    char status;
    char fit;
    int start;
    int size;
    int next;
    char name[16];
}ExtendBootRecord;

extern void crearDisco(int tamanio, char* ruta, char* unidad);
extern void eliminarDiscos(char* ruta);
extern void crearParticiones(int size, char* ruta, char* name, char* unit, char* type, char* fit);
extern void formatearParticion(char* name, char* path, char* tipo);
extern int tamanioLibreDisco(char* ruta);


#endif // DISCOS_H

