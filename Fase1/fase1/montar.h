#ifndef MONTAR_H
#define MONTAR_H
#include "discos.h"

typedef struct discos{
    char path[150];
    char letra;
    char numero;
}discos;

typedef struct particiones{
    char id[5];
    char path[150];
    char name[32];
    int estado;
}particiones;

extern void montarParticion(char* path, char* name);
extern void desmontarParticion(char* id);
extern void imprimirUnidades();
extern particiones verificarID(char* id);
extern void desmontarParticionDos(char* name, char* path);

#endif // MONTAR_H

