#include "reportes.h"
#include "montar.h"
#include "discos.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


void reporteDisco(char* path, char* id){
    particiones parti;
    parti = verificarID(id);

    if(strcmp(parti.path,"null")==0)printf("\n***EL ID INDICADO NO A SIDO ASIGNADO***\n");
    else{
        FILE* disco;
        disco = fopen(parti.path,"r");
        if(disco==NULL)printf("\n***ERROR AL ABRIR EL DISCO***\n");
        else{
            MasterBootRecord mbr;
            fseek(disco,0,SEEK_SET);
            fread(&mbr,sizeof(mbr),1,disco);

            FILE* reporte;
            reporte = fopen("temporal.txt","w");

            if(reporte == NULL)printf("\n***ERROR AL CREAR EL REPORTE***\n");
            else{
                fprintf(reporte,"graph G {\n");
                fprintf(reporte,"\trankdir = TR;\n\n");
                fprintf(reporte,"\tgraph [bgcolor=\"#A9E2F3\"];\n");
                fprintf(reporte,"\tnode [shape=record style=filled fillcolor=\"#00ff005f\"]\n\n");
                fprintf(reporte,"\tsubgraph cluster0 {\n");
                fprintf(reporte,"\t\tlabel = \"REPORTE DE DISCOS\";\n");
                fprintf(reporte,"\t\tcolor=black;\n");

                int i; int tamLibre = 0;
                tamLibre = tamanioLibreDisco(parti.path);

                if(tamLibre>0) fprintf(reporte,"\t\tLIBRE [label = \"LIBRE\" ];\n");

                for(i=3; i>=0; i-- ){
                    int libre=0;
                    if(i<3){
                        libre= mbr.particones[i].start+mbr.particones[i].size -mbr.particones[i+1].start;
                    }
                    if(mbr.particones[i].status == '1'){
                        if(mbr.particones[i].type == 'e'){
                            fprintf(reporte,"\t\tparti1_%i [label = \"EXTENDIDA\" ];\n",i);
                            if(libre>0)if(libre>0)fprintf(reporte,"\t\tLIBRE%i [label = \"LIBRE\" ];\n",i);

                        }else{
                            fprintf(reporte,"\t\tparti1_%i [label = \"PRIMARIA\" ];\n",i);;
                            if(libre>0)if(libre>0)fprintf(reporte,"\t\tLIBRE%i [label = \"LIBRE\" ];\n",i);
                        }
                    }
          /*
                    if(mbr.particones[i].type == 'e'){
                        fprintf(reporte,"\t\tparti1_%i[label = \"EXTENDIDA\" ] ;\n",i);
                    }
                    if(mbr.particones[i].type == 'l'){
                        fprintf(reporte,"\t\tLOGICA;\n");
                    }
                    */
                }

                fprintf(reporte,"\t\tMBR;\n\n");
                fprintf(reporte,"\t}\n");
                fprintf(reporte,"}\n");

                fclose(reporte);
                fclose(disco);

                system("dot -Tpng temporal.txt -o temporal.png");
                system("eog temporal.png");
                printf("\n***REPORTE CREADO CON EXITO****\n");
            }
        }
    }
}

void reporteMBR(char* path, char* id){
    particiones parti;
    parti = verificarID(id);

    if(strcmp(parti.path,"null")==0)printf("\n***EL ID INDICADO NO A SIDO ASIGNADO***\n");
    else{
        FILE* disco;
        disco = fopen(parti.path,"r");

        if(disco == NULL)printf("\n***ERROR AL ABRIR EL DISCO***\n");
        else{
            MasterBootRecord mbr;
            fseek(disco,0,SEEK_SET);
            fread(&mbr,sizeof(mbr),1,disco);

            FILE* reporte;
            reporte = fopen("temporal2.txt","w");

            if(reporte == NULL)printf("\n***ERROR AL CREAR EL REPORTE***\n");
            else{
                fprintf(reporte,"digraph G {\n\n");
                fprintf(reporte,"node [shape=plaintext]\n");
                fprintf(reporte,"a [label=<<table border=\"0\" cellborder=\"1\" cellspacing=\"0\">\n\n");
                fprintf(reporte,"<tr><td><b>Nombre</b></td><td><b>Valor</b></td></tr>\n");
                fprintf(reporte,"<tr><td> MBR_TAMANIO         </td><td> %i </td></tr>\n",mbr.mbr_tamanio);
                fprintf(reporte,"<tr><td> MBR_FECHA_CREACION  </td><td> %s </td></tr>\n","FECHA");
                fprintf(reporte,"<tr><td> MBR_DISK_ASIGNATURE </td><td> %i </td></tr>\n",mbr.mbr_asignature);
                int i; int cont =1;
                for(i=0; i<4; i++){
                    Partition parti;
                    parti = mbr.particones[i];
                //    if(parti.status=='1'){
                        fprintf(reporte,"<tr><td colspan=\"2\"><b> PARTICION %i</b></td></tr>",cont);
                        fprintf(reporte,"<tr><td> PART_STATUS%i </td><td> %c </td></tr>\n",cont,parti.status);
                        fprintf(reporte,"<tr><td> PART_TYPE%i   </td><td> %c </td></tr>\n",cont,parti.type);
                        fprintf(reporte,"<tr><td> PART_FIT%i    </td><td> %c </td></tr>\n",cont,parti.fit);
                        fprintf(reporte,"<tr><td> PART_START%i  </td><td><font color=\"red\">%i </font></td></tr>\n",cont,parti.start);
                        fprintf(reporte,"<tr><td> PART_SIZE%i   </td><td> %i </td></tr>\n",cont,parti.size);
                        fprintf(reporte,"<tr><td> PART_NAME%i   </td><td> %s </td></tr>\n",cont,parti.name);
                        cont++;
                //    }
                }


                fprintf(reporte,"</table>>];\n\n");
                fprintf(reporte,"}");

                fclose(disco);
                fclose(reporte);

                system("dot -Tpng temporal2.txt -o temporal2.png");
                system("eog temporal2.png");
                printf("\n***REPORTE CREADO CON EXITO****\n");
            }
        }
    }

}
